// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    repositories {
        google()
        mavenCentral()
        maven("https://jcenter.bintray.com/")
    }
    dependencies {
        classpath(Libs.Classpath.ANDROID_TOOLS_GRADLE)
        classpath(Libs.Classpath.KOTLIN)
        classpath(Libs.Classpath.GOOGLE_SERVICES)
        classpath(Libs.Classpath.FIREBASE_CRASHLYTICS)
        classpath(Libs.Classpath.JACOCO)
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
        maven("https://jcenter.bintray.com/")
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}