# [rec-2022043002]
## Added
- New SplashScreen API
- Show AlertDialog when not internet available on launch
## Removed
- SplashScreenActivity
- Old splash screen image

# [rec-2022043001]
## Updated
- Launch icons
## Removed
- Night theme created by default by AS

# [rec-2022041804]
## Updated
- "kotlinOptions.jvmTarget" to 11
## Fixed
- CI

# [rec-2022041803]
## Updated
- Versions : Build Tools, Tools R8, Tools Gradle, Gradle Wrapper, Kotlin

# [rec-2022041802]
## Added
- ClickableViewData to handle onClick on Views

# [rec-2022041801]
## Updated
- Use <include> for layout "view_last_change"
## Removed
- LastChangeView

# [rec-2022041701]
## Added
_ IRouter and it's implementation
- AppCoordinator
- MainActivityViewModel
## Updated
- Showing "OverviewFragment" using IRouter and AppCoordinator

# [rec-2022041001]
## Added
- Secure API Keys using NDK (Native Development Kit)

# [rec-2022040601]
## Added
- Plugin "com.github.ben-manes.versions"

# [rec-2022040501]
## Updated
- Target SDK version 31
- Replace the library Chuck by Chucker

# [rec-2022040401]
## Added
- HTTP requests/responses content inspection on in-app UI

# [1.21.0]
## Added
- IResourcesRepository implementation
- TestMonitor
- Mock Monitor
## Removed
- BaseUnitTest

# [1.20.0]
## Added
- mavenCentral
- TODO list file
## Removed
- Jcenter
## Updated
- Kotlin version
- Make rec build variant selected by default

# [1.19.0]
## Added
- Static object `Monitor`

# [1.18.0]
## Added
- Handle SSL : Pinning certificate

# [1.17.0]
## Added
- Jacoco and code coverage

# [1.16.0]
## Added
- Integrate linter

# [1.15.0]
## Added
- Gitlab CI
## Added
- Move API_KEY to gradle.properties instead of local.properties to fix gitlab ci build

# [1.14.0]
## Added
- NumberUtils class

# [1.13.0]
## Added
- Unit tests for OverviewRepository
- DummyCall : A simple class to test Retrofit calls.
- InstantExecutorExtension : A Junit5 extension for coroutines and default Android Executor
- Unit tests

# [1.12.0]
## Added
- UI components docs

# [1.11.0]
## Updated
- Make RepositoryResult to take a generic class of Success

# [1.10.0]
## Fixed
- quickCallApi

# [1.9.0]
## Fixed
- Set is_mock_enabled as false for PROD environment
- Fix mock
- Remove all utils class for Retrofit calls under `data/api/call` package

# [1.8.0]
## Added
- Create BaseView so that all other views inherit from it

# [1.7.0]
## Added
- Custom key and user id when upload crashes on Firebase

# [1.6.0]
## Added
- Setup crashlytics and analytics
- Setup Timber plant tree for debug and release
- Create key store and setup signing config
## Updated
- OkHttpLogging with IMonitor instead of default logger

# [1.5.0]
## Added
- Show last change time on OverviewFragment

# [1.4.0]
## Added
- SwipeRefreshLayout on OverviewFragment to handle refresh action

# [1.3.0]
## Added
- Safe call API

# [1.2.0]
## Fixed
- Initialize views before call load method

# [1.1.0]
## Added
- Mock interceptor

# [1.0.0]
## Added
- Initial commit of project