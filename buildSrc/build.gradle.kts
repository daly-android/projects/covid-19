plugins{
    `kotlin-dsl`
}

repositories {
    google()
    mavenCentral()
    maven("https://jcenter.bintray.com/")
}