import org.gradle.api.artifacts.dsl.DependencyHandler

private const val KAPT = "kapt"
private const val IMPLEMENTATION = "implementation"
private const val TEST_IMPLEMENTATION = "testImplementation"
private const val ANDROID_TEST_IMPLEMENTATION = "androidTestImplementation"
private const val KTLINT = "ktlint"

object Dependencies {

    val appLibraries = arrayListOf<String>().apply {

        // core-app dependencies
        add(Libs.Kotlin.KOTLIN_STDLIB)
        add(Libs.Android.CORE_KTX)
        add(Libs.Android.APP_COMPAT)
        add(Libs.Android.LIFECYCLE)
        add(Libs.Android.LIFECYCLE_VIEWMODEL)
        add(Libs.Android.LIFECYCLE_LIVEDATA)
        add(Libs.Android.FRAGMENT)
        add(Libs.Android.SPLASH_SCREEN)

        // Kotlin dependencies
        add(Libs.Kotlin.KOTLIN_REFLECT)
        add(Libs.Kotlin.KOTLIN_STDLIB)
        add(Libs.Kotlin.KOTLIN_STDLIB_COMMON)
        add(Libs.Kotlin.KOTLIN_STDLIB_JDK_7)
        add(Libs.Kotlin.KOTLIN_STDLIB_JDK_8)

        // Logging dependencies
        add(Libs.Logging.TIMBER)

        // Monitoring dependencies
        add(Libs.Monitoring.FIREBASE_ANALYTICS)
        add(Libs.Monitoring.FIREBASE_CRASHLYTICS)

        // UI dependencies
        add(Libs.UI.MATERIAL)
        add(Libs.UI.CONSTRAINT_LAYOUT)
        add(Libs.UI.SWIPE_REFRESH_LAYOUT)

        // Api dependencies
        add(Libs.Api.THREETENBP_ADAPTER)
        add(Libs.Api.JSON_MOSHI_KOTLIN)
        add(Libs.Api.JSON_MOSHI_ADAPTER)

        // Network dependencies
        add(Libs.Network.RETROFIT)
        add(Libs.Network.RETROFIT_CONVERTER_JSON)
        add(Libs.Network.RETROFIT_CONVERTER_SCALARS)
        add(Libs.Network.HTTP_LOGGING_INTERCEPTOR)
        add(Libs.Network.CHUCKER)

        // Dependency-injection dependencies
        add(Libs.DependencyInjection.KOIN_CORE)
        add(Libs.DependencyInjection.KOIN_ANDROID)
        add(Libs.DependencyInjection.KOIN_ANDROID_SCOPE)
        add(Libs.DependencyInjection.KOIN_ANDROID_VIEWMODEL)
        add(Libs.DependencyInjection.KOIN_ANDROID_EXT)
        add(Libs.DependencyInjection.KOIN_ANDROID_FRAGMENT)

        // Asynchronous dependencies
        add(Libs.Asynchronous.COROUTINES_ANDROID)

        // Others dependencies
        add(Libs.Others.PODAM)
        add(Libs.Others.GSON)
    }

    val platformLibraries = arrayListOf<String>().apply {
        add(Libs.Monitoring.FIREBASE_BOM)
    }

    val dataBindingLibraries = arrayListOf<String>().apply {
        add(Libs.Android.DATABINDING)
    }

    val testLibraries = arrayListOf<String>().apply {
        add(Libs.Testing.CORE)
        add(Libs.Testing.MOCKK)
        //add(Libs.Testing.TRUTH)
        add(Libs.Testing.JUNIT5)
        add(Libs.Testing.JUNIT5_PARAMS)
        add(Libs.Testing.COROUTINES)
    }
}

// Util functions for adding the different type dependencies from build.gradle.kts file
fun DependencyHandler.kapt(list: List<String>) {
    list.forEach { dependency ->
        add(KAPT, dependency)
    }
}

fun DependencyHandler.implementation(list: List<String>) {
    list.forEach { dependency ->
        add(IMPLEMENTATION, dependency)
    }
}

fun DependencyHandler.implementationPlatform(list: List<String>) {
    list.forEach { dependency ->
        add(IMPLEMENTATION, platform(dependency))
    }
}

fun DependencyHandler.androidTestImplementation(list: List<String>) {
    list.forEach { dependency ->
        add(ANDROID_TEST_IMPLEMENTATION, dependency)
    }
}

fun DependencyHandler.testImplementation(list: List<String>) {
    list.forEach { dependency ->
        add(TEST_IMPLEMENTATION, dependency)
    }
}