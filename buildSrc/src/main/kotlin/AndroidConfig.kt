object AndroidConfig {
    const val BUILD_TOOLS_VERSION = "31.0.0"
    const val MIN_SDK_VERSION = 23
    const val TARGET_SDK_VERSION = 31
    const val COMPILE_SDK_VERSION = 31
    const val APPLICATION_ID = "com.daly.covid19"
    const val ANDROID_TEST_INSTRUMENTATION = "androidx.test.runner.AndroidJUnitRunner"
}