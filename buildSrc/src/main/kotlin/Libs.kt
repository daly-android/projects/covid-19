object Libs {

    object Android {
        const val APP_COMPAT = "androidx.appcompat:appcompat:${Versions.Android.APP_COMPAT}"
        const val CORE_KTX = "androidx.core:core-ktx:${Versions.Android.CORE_KTX}"
        const val LIFECYCLE = "androidx.lifecycle:lifecycle-extensions:${Versions.Android.LIFECYCLE_EXTENSIONS}"
        const val LIFECYCLE_VIEWMODEL = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.Android.LIFECYCLE}"
        const val LIFECYCLE_LIVEDATA = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.Android.LIFECYCLE}"
        const val FRAGMENT = "androidx.fragment:fragment-ktx:${Versions.Android.FRAGMENT}"
        const val DATABINDING = "com.android.databinding:compiler:${Versions.Android.DATABINDING}"
        const val SPLASH_SCREEN = "androidx.core:core-splashscreen:${Versions.Android.SPLASH_SCREEN}"
    }

    object Api {
        const val JSON_MOSHI_KOTLIN = "com.squareup.moshi:moshi-kotlin:${Versions.Api.JSON_MOSHI_KOTLIN}"
        const val JSON_MOSHI_ADAPTER = "com.squareup.moshi:moshi-adapters:${Versions.Api.JSON_MOSHI_ADAPTER}"
        const val THREETENBP_ADAPTER = "org.threeten:threetenbp:${Versions.Api.THREETENBP_ADAPTER}"
    }

    object Classpath {
        const val ANDROID_TOOLS_R8 = "com.android.tools:r8:${Versions.Classpath.ANDROID_TOOLS_R8}"
        const val ANDROID_TOOLS_GRADLE = "com.android.tools.build:gradle:${Versions.Classpath.ANDROID_TOOLS_GRADLE}"
        const val KOTLIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.Kotlin.KOTLIN_VERSION}"
        const val JACOCO = "org.jacoco:org.jacoco.core:${Versions.Classpath.JACOCO}"
        const val KOIN = "org.koin:koin-gradle-plugin:${Versions.DependencyInjection.KOIN}"
        const val JUNIT_5 = "de.mannodermaus.gradle.plugins:android-junit5:${Versions.Classpath.JUNIT_5}"
        const val GOOGLE_SERVICES = "com.google.gms:google-services:${Versions.Classpath.GOOGLE_SERVICES}"
        const val FIREBASE_CRASHLYTICS = "com.google.firebase:firebase-crashlytics-gradle:${Versions.Classpath.FIREBASE_CRASHLYTICS}"
    }

    object Asynchronous {
        const val COROUTINES_ANDROID = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.Asynchronous.COROUTINES}"
        const val COROUTINES_CORE = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.Asynchronous.COROUTINES}"
    }

    object DependencyInjection {
        const val KOIN_CORE = "org.koin:koin-core:${Versions.DependencyInjection.KOIN}"
        const val KOIN_ANDROID = "org.koin:koin-android:${Versions.DependencyInjection.KOIN}"
        const val KOIN_ANDROID_SCOPE = "org.koin:koin-android-scope:${Versions.DependencyInjection.KOIN}"
        const val KOIN_ANDROID_VIEWMODEL = "org.koin:koin-android-viewmodel:${Versions.DependencyInjection.KOIN}"
        const val KOIN_ANDROID_EXT = "org.koin:koin-android-ext:${Versions.DependencyInjection.KOIN}"
        const val KOIN_ANDROID_FRAGMENT = "org.koin:koin-androidx-fragment:${Versions.DependencyInjection.KOIN}"
    }

    object Kotlin {
        const val KOTLIN_STDLIB_JDK_8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.Kotlin.KOTLIN_VERSION}"
        const val KOTLIN_STDLIB_JDK_7 = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.Kotlin.KOTLIN_VERSION}"
        const val KOTLIN_REFLECT = "org.jetbrains.kotlin:kotlin-reflect:${Versions.Kotlin.KOTLIN_VERSION}"
        const val KOTLIN_STDLIB = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.Kotlin.KOTLIN_VERSION}"
        const val KOTLIN_STDLIB_COMMON = "org.jetbrains.kotlin:kotlin-stdlib-common:${Versions.Kotlin.KOTLIN_VERSION}"
        const val KOTLIN_GRADLE_PLUGIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.Kotlin.KOTLIN_VERSION}"
    }

    object Lint {
        const val KTLINT = "com.pinterest:ktlint:${Versions.Lint.KTLINT}"
    }

    object Logging {
        const val TIMBER = "com.jakewharton.timber:timber:${Versions.Logging.TIMBER}"
    }

    object Monitoring {
        // When using the BoM, you don't specify versions in Firebase library dependencies
        const val FIREBASE_BOM = "com.google.firebase:firebase-bom:${Versions.Monitoring.FIREBASE_BOM}"
        const val FIREBASE_ANALYTICS = "com.google.firebase:firebase-analytics-ktx"
        const val FIREBASE_CRASHLYTICS = "com.google.firebase:firebase-crashlytics-ktx"
        const val FIREBASE_MESSAGING = "com.google.firebase:firebase-messaging-ktx"
    }

    object Network {
        const val RETROFIT = "com.squareup.retrofit2:retrofit:${Versions.Network.RETROFIT}"
        const val RETROFIT_CONVERTER_JSON = "com.squareup.retrofit2:converter-moshi:${Versions.Network.RETROFIT}"
        const val RETROFIT_CONVERTER_SCALARS = "com.squareup.retrofit2:converter-scalars:${Versions.Network.RETROFIT}"
        const val HTTP_LOGGING_INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor:${Versions.Network.LOGGING_INTERCEPTOR}"
        const val CHUCKER = "com.github.chuckerteam.chucker:library:${Versions.Network.CHUCKER}"// https://github.com/ChuckerTeam/chucker/blob/develop/README.md
    }

    object Others {
        const val PODAM = "uk.co.jemos.podam:podam:${Versions.Others.PODAM}"
        const val GSON = "com.google.code.gson:gson:${Versions.Others.GSON}"
    }

    object Plugins {
        const val GRADLE_VERSIONS = "com.github.ben-manes.versions"
    }

    object Testing {
        const val CORE = "androidx.arch.core:core-runtime:${Versions.Testing.CORE_TESTING}"
        const val JACOCO = "org.jacoco:org.jacoco.core:${Versions.Testing.JACOCO}"
        const val JUNIT5 = "org.junit.jupiter:junit-jupiter:${Versions.Testing.JUNIT5}"
        const val JUNIT5_PARAMS = "org.junit.jupiter:junit-jupiter-params:${Versions.Testing.JUNIT5}"

        const val MOCKK = "io.mockk:mockk:${Versions.Testing.MOCKK}"
        const val TRUTH = "com.google.truth:truth:${Versions.Testing.GOOGLE_TRUTH}"
        const val SONARQUBE = "org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:${Versions.Testing.SONARQUBE}"
        const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.Testing.COROUTINES}"

        const val KOIN_TEST = "org.koin:koin-test:${Versions.DependencyInjection.KOIN}"
    }

    object UI {
        const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:${Versions.UI.CONSTRAINT_LAYOUT}"
        const val MATERIAL = "com.google.android.material:material:${Versions.UI.MATERIAL}"
        const val SWIPE_REFRESH_LAYOUT = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.UI.SWIPE_REFRESH_LAYOUT}"
    }
}