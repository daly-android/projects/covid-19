package com.daly.covid19

import android.annotation.SuppressLint
import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.BeforeEachCallback
import org.junit.jupiter.api.extension.ExtensionContext

/**
 * A Junit5 extension for coroutines and default Android Executor
 * Usage : `@ExtendWith(Junit5CoroutineExtension::class)` on a Junit5 UnitTest class
 * */
@ExperimentalCoroutinesApi
@SuppressLint("RestrictedApi")
class InstantExecutorExtension(
    private val dispatcher: TestCoroutineDispatcher = TestCoroutineDispatcher()
) : BeforeAllCallback, BeforeEachCallback, AfterAllCallback, AfterEachCallback, TestCoroutineScope by TestCoroutineScope(dispatcher) {

    inner class ArchExecutor : TaskExecutor() {
        override fun executeOnDiskIO(runnable: Runnable) = runnable.run()
        override fun postToMainThread(runnable: Runnable) = runnable.run()
        override fun isMainThread(): Boolean = true
    }

    override fun beforeAll(context: ExtensionContext?) {
        ArchTaskExecutor.getInstance().setDelegate(ArchExecutor())
        Dispatchers.setMain(dispatcher)
    }

    override fun afterAll(context: ExtensionContext?) {
        Dispatchers.resetMain()
        dispatcher.cleanupTestCoroutines()
        ArchTaskExecutor.getInstance().setDelegate(null)
    }

    override fun beforeEach(context: ExtensionContext?) {
        mockMonitorLogging()
    }

    override fun afterEach(context: ExtensionContext?) {
        cleanupTestCoroutines()
    }
}