package com.daly.covid19.databuilder

import com.daly.covid19.overview.domain.models.LatestTotalsModel

object DataBuilderCategoryInfo {

    // Default value for test
    const val CONFIRMED = 123456789L
    const val RECOVERED = 123478L
    const val CRITICAL = 123456L
    const val DEATHS = 567890L

    fun create(
        category: LatestTotalsModel.Category = LatestTotalsModel.Category.CONFIRMED,
        value: Long = CONFIRMED
    ) = LatestTotalsModel.CategoryInfo(
        category = category,
        value = value
    )

    fun createList() = listOf(
        LatestTotalsModel.CategoryInfo(category = LatestTotalsModel.Category.CONFIRMED, value = CONFIRMED),
        LatestTotalsModel.CategoryInfo(category = LatestTotalsModel.Category.RECOVERED, value = RECOVERED),
        LatestTotalsModel.CategoryInfo(category = LatestTotalsModel.Category.CRITICAL, value = CRITICAL),
        LatestTotalsModel.CategoryInfo(category = LatestTotalsModel.Category.DEATHS, value = DEATHS),
    )
}