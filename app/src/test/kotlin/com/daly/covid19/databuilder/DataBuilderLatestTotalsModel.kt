package com.daly.covid19.databuilder

import com.daly.covid19.overview.domain.models.LatestTotalsModel
import com.daly.covid19.overview.domain.models.LatestTotalsModel.CategoryInfo

object DataBuilderLatestTotalsModel {

    // Default value for test
    const val LAST_CHANGE = "2021-04-28T17:57:49+02:00"
    const val LAST_UPDATE = "2021-04-28T18:00:04+02:00"

    fun create(
        categoryInfo: List<CategoryInfo> = DataBuilderCategoryInfo.createList(),
        lastChange: String = LAST_CHANGE,
        lastUpdate: String = LAST_UPDATE
    ) = LatestTotalsModel(
        categoryInfo,
        lastChange,
        lastUpdate
    )
}