package com.daly.covid19.overview.data.models

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class LatestTotalsUnitTest {

    private lateinit var latestTotals: LatestTotals

    companion object {
        const val CONFIRMED = 123456789L
        const val RECOVERED = 123456789L
        const val CRITICAL = 123456L
        const val DEATHS = 567890L
        const val LAST_CHANGE = "2021-04-28T17:57:49+02:00"
        const val LAST_UPDATE = "2021-04-28T18:00:04+02:00"
    }

    @Test
    fun testInitializationLatestTotals() {
        // when
        latestTotals = LatestTotals(
            CONFIRMED, RECOVERED, CRITICAL, DEATHS, LAST_CHANGE, LAST_UPDATE
        )

        // then
        assertEquals(CONFIRMED, latestTotals.confirmed)
        assertEquals(RECOVERED, latestTotals.recovered)
        assertEquals(CRITICAL, latestTotals.critical)
        assertEquals(DEATHS, latestTotals.deaths)
        assertEquals(LAST_CHANGE, latestTotals.lastChange)
        assertEquals(LAST_UPDATE, latestTotals.lastUpdate)
    }
}