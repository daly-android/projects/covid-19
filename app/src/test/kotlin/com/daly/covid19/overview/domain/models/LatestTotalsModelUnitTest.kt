package com.daly.covid19.overview.domain.models

import com.daly.covid19.databuilder.DataBuilderCategoryInfo
import com.daly.covid19.databuilder.DataBuilderLatestTotalsModel
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class LatestTotalsModelUnitTest {

    private lateinit var latestTotalsModel: LatestTotalsModel

    @Test
    fun testInitialisationLatestTotalsModel() {
        // given
        latestTotalsModel = LatestTotalsModel(
            categories = DataBuilderCategoryInfo.createList(),
            lastChange = DataBuilderLatestTotalsModel.LAST_CHANGE,
            lastUpdate = DataBuilderLatestTotalsModel.LAST_UPDATE
        )

        // then
        assertEquals(DataBuilderLatestTotalsModel.LAST_CHANGE, latestTotalsModel.lastChange)
        assertEquals(DataBuilderLatestTotalsModel.LAST_UPDATE, latestTotalsModel.lastUpdate)
        assertEquals(LatestTotalsModel.Category.CONFIRMED, latestTotalsModel.categories[0].category)
        assertEquals(DataBuilderCategoryInfo.CONFIRMED, latestTotalsModel.categories[0].value)

        assertEquals(LatestTotalsModel.Category.RECOVERED, latestTotalsModel.categories[1].category)
        assertEquals(DataBuilderCategoryInfo.RECOVERED, latestTotalsModel.categories[1].value)

        assertEquals(LatestTotalsModel.Category.CRITICAL, latestTotalsModel.categories[2].category)
        assertEquals(DataBuilderCategoryInfo.CRITICAL, latestTotalsModel.categories[2].value)

        assertEquals(LatestTotalsModel.Category.DEATHS, latestTotalsModel.categories[3].category)
        assertEquals(DataBuilderCategoryInfo.DEATHS, latestTotalsModel.categories[3].value)
    }
}