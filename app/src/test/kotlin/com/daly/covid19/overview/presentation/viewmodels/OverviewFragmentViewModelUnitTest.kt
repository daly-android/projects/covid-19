package com.daly.covid19.overview.presentation.viewmodels

import com.daly.covid19.InstantExecutorExtension
import com.daly.covid19.R
import com.daly.covid19.common.core.interfaces.IResourcesRepository
import com.daly.covid19.common.extensions.asFlow
import com.daly.covid19.common.utils.CategoryUtils
import com.daly.covid19.databuilder.DataBuilderCategoryInfo
import com.daly.covid19.databuilder.DataBuilderLatestTotalsModel
import com.daly.covid19.overview.domain.repositories.IOverviewRepository
import com.daly.covid19.overview.domain.repositories.RepositoryError
import com.daly.covid19.overview.domain.repositories.RepositoryResult
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
class OverviewFragmentViewModelUnitTest {

    companion object {
        private const val CONFIRMED = "Confirmed"
        private const val RECOVERED = "Recovered"
        private const val CRITICAL = "Critical"
        private const val DEATHS = "Deaths"
        private const val UNAVAILABLE = "Unavailable"
    }

    private val overviewRepository = mockk<IOverviewRepository>()

    @RelaxedMockK
    private lateinit var resourcesRepository: IResourcesRepository

    private lateinit var viewModel: OverviewFragmentViewModel

    @BeforeEach
    fun before() {
        every { resourcesRepository.fetchString(R.string.confirmed) } answers { CONFIRMED }
        every { resourcesRepository.fetchString(R.string.recovered) } answers { RECOVERED }
        every { resourcesRepository.fetchString(R.string.critical) } answers { CRITICAL }
        every { resourcesRepository.fetchString(R.string.deaths) } answers { DEATHS }
        every { resourcesRepository.fetchString(R.string.unavailable) } answers { UNAVAILABLE }

        viewModel = spyk(OverviewFragmentViewModel(resourcesRepository, overviewRepository), recordPrivateCalls = true)
    }

    @Test
    @DisplayName("Default values")
    fun testDefaultValues() {
        assertEquals(CONFIRMED, viewModel.confirmedCases.value?.categoryTitle?.value)
        assertEquals(RECOVERED, viewModel.recoveredCases.value?.categoryTitle?.value)
        assertEquals(CRITICAL, viewModel.criticalCases.value?.categoryTitle?.value)
        assertEquals(DEATHS, viewModel.deathsCases.value?.categoryTitle?.value)

        assertEquals(UNAVAILABLE, viewModel.confirmedCases.value?.categoryValue?.value)
        assertEquals(UNAVAILABLE, viewModel.recoveredCases.value?.categoryValue?.value)
        assertEquals(UNAVAILABLE, viewModel.criticalCases.value?.categoryValue?.value)
        assertEquals(UNAVAILABLE, viewModel.deathsCases.value?.categoryValue?.value)

        assertNotNull(viewModel.confirmedCases)
        assertNotNull(viewModel.recoveredCases)
        assertNotNull(viewModel.criticalCases)
        assertNotNull(viewModel.deathsCases)
        assertNotNull(viewModel.inProgress)
    }

    @Test
    @DisplayName("Load with error : Repository return null")
    fun loadErrorNull() = runBlocking {
        // given
        val progressFlow = viewModel.inProgress.asFlow(3) // FIXME : KO when use runBlockingTest
        every { overviewRepository.fetchLatestTotals() } returns flow { emit(null) }

        // when
        viewModel.load().join()
        // viewModel.load()

        // then
        assertEquals(listOf(false, true, false), progressFlow.toList())
        assertNull(viewModel.latestTotalsCases.value)
        verify {
            viewModel invokeNoArgs "handleGenericError"
        }
    }

    @Test
    @DisplayName("Load with error : Not authorized error")
    fun loadNotAuthorizedError() = runBlocking {
        // given
        val progressFlow = viewModel.inProgress.asFlow(3)
        every { overviewRepository.fetchLatestTotals() } returns flow { emit(RepositoryResult.Failure(RepositoryError.NotAuthorized)) }

        // when
        viewModel.load()

        // then
        assertEquals(listOf(false, true, false), progressFlow.toList())
        assertNull(viewModel.latestTotalsCases.value)
        verify {
            viewModel invokeNoArgs "handleNotAuthorizedError"
        }
    }

    @Test
    @DisplayName("Load with error : Internal error")
    fun loadInternalError() = runBlocking {
        // given
        val progressFlow = viewModel.inProgress.asFlow(3)
        every { overviewRepository.fetchLatestTotals() } returns flow { emit(RepositoryResult.Failure(RepositoryError.Internal)) }

        // when
        viewModel.load()

        // then
        assertEquals(listOf(false, true, false), progressFlow.toList())
        assertNull(viewModel.latestTotalsCases.value)
        verify {
            viewModel invokeNoArgs "handleInternalError"
        }
    }

    @Test
    @DisplayName("Load latest totals succeed")
    fun loadSuccess() = runBlocking {
        // given
        val progressFlow = viewModel.inProgress.asFlow(3)
        every { overviewRepository.fetchLatestTotals() } returns flow { emit(RepositoryResult.Success(DataBuilderLatestTotalsModel.create())) }

        // when
        viewModel.load().join()

        // then
        assertEquals(listOf(false, true, false), progressFlow.toList())
        assertNotNull(viewModel.latestTotalsCases.value)
        assertEquals(4, viewModel.latestTotalsCases.value!!.size)
        assertEquals(DataBuilderLatestTotalsModel.LAST_CHANGE, viewModel.lastChange.lastChangeText.value)
        assertEquals(
            CategoryUtils.insertSpacesBetweenNumbers(DataBuilderCategoryInfo.CONFIRMED.toString()),
            viewModel.confirmedCases.value?.categoryValue?.value
        )
        assertEquals(
            CategoryUtils.insertSpacesBetweenNumbers(DataBuilderCategoryInfo.RECOVERED.toString()),
            viewModel.recoveredCases.value?.categoryValue?.value
        )
        assertEquals(CategoryUtils.insertSpacesBetweenNumbers(DataBuilderCategoryInfo.CRITICAL.toString()), viewModel.criticalCases.value?.categoryValue?.value)
        assertEquals(CategoryUtils.insertSpacesBetweenNumbers(DataBuilderCategoryInfo.DEATHS.toString()), viewModel.deathsCases.value?.categoryValue?.value)
    }
}