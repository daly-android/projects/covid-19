package com.daly.covid19.overview.data.mappers

import com.daly.covid19.overview.data.models.LatestTotals
import com.daly.covid19.overview.domain.models.LatestTotalsModel.Category
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class LatestTotalsApiMapperUnitTest {

    companion object {
        const val CONFIRMED = 123456789L
        const val RECOVERED = 123456789L
        const val CRITICAL = 123456L
        const val DEATHS = 567890L
        const val ZERO_LONG = 0L
        const val LAST_CHANGE = "2021-04-28T17:57:49+02:00"
        const val LAST_UPDATE = "2021-04-28T18:00:04+02:00"
    }

    private fun createLatestTotals(confirmed: Long?, recovered: Long?, critical: Long?, deaths: Long?, lastChange: String?, lastUpdate: String?) =
        LatestTotals(confirmed, recovered, critical, deaths, lastChange, lastUpdate)

    @Test
    fun testLatestTotalsToNullDomainModel() {
        // given
        val latestTotalsNull = createLatestTotals(null, null, null, null, null, null)

        // when
        val mapApiResult = latestTotalsNull.toDomainModel()

        // then
        assertEquals(ZERO_LONG, mapApiResult.categories[0].value)
        assertEquals(ZERO_LONG, mapApiResult.categories[1].value)
        assertEquals(ZERO_LONG, mapApiResult.categories[2].value)
        assertEquals(ZERO_LONG, mapApiResult.categories[3].value)
        assertEquals(null, mapApiResult.lastChange)
        assertEquals(null, mapApiResult.lastUpdate)
    }

    @Test
    fun testLatestTotalsToDomainModel() {
        // given
        val latestTotals = createLatestTotals(CONFIRMED, RECOVERED, CRITICAL, DEATHS, LAST_CHANGE, LAST_UPDATE)

        // when
        val mapApiResult = latestTotals.toDomainModel()

        // then
        assertEquals(CONFIRMED, mapApiResult.categories[0].value)
        assertEquals(Category.CONFIRMED, mapApiResult.categories[0].category)

        assertEquals(RECOVERED, mapApiResult.categories[1].value)
        assertEquals(Category.RECOVERED, mapApiResult.categories[1].category)

        assertEquals(CRITICAL, mapApiResult.categories[2].value)
        assertEquals(Category.CRITICAL, mapApiResult.categories[2].category)

        assertEquals(DEATHS, mapApiResult.categories[3].value)
        assertEquals(Category.DEATHS, mapApiResult.categories[3].category)

        assertEquals(LAST_CHANGE, mapApiResult.lastChange)
        assertEquals(LAST_UPDATE, mapApiResult.lastUpdate)
    }
}