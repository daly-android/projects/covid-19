# For more information, see https://developer.android.com/ndk/guides/android_mk
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := keys
LOCAL_SRC_FILES := keys.c
LOCAL_LDLIBS := -llog

include $(BUILD_SHARED_LIBRARY)