package com.daly.covid19.common.mock

import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.PUT

/**
 * Utils to manage type using reflection
 */
object RetrofitTypeUtils {

    private const val BRACES_BLOCK = "\\{.*?\\}"
    private const val METHOD_GET = "GET"
    private const val METHOD_POST = "POST"
    private const val METHOD_PUT = "PUT"
    private const val METHOD_PATCH = "PATCH"
    private const val METHOD_DELETE = "DELETE"
    private const val PATH_ARGUMENTS_SEPARATOR = "?"
    private const val PATH_SEPARATOR = "/"

    /**
     * Get expected type from method in [apiClass], searching with [requestPath] and [requestHttpMethod]
     */
    fun getExpectedTypeResultFromUri(apiClass: Class<*>, requestPath: String, requestHttpMethod: String): Type? {
        // val requestPathNoGetParams = requestPath.split(PATH_ARGUMENTS_SEPARATOR)[0]
        // get retrofit2.Call<XXX> where XXX is expected model result
        // FIXME : should pass requestPathNoGetParams instead of requestPath
        //   but when pass requestPathNoGetParams, isUrlMatch() return false
        val method = getMethod(apiClass, requestPath, requestHttpMethod) ?: return null
        val type = method.genericReturnType as ParameterizedType

        // get expected model result
        return if (type.actualTypeArguments.size == 1) type.actualTypeArguments[0] else null
    }

    /**
     * Get method of [apiClass] that correspond to [requestPath] and [requestHttpMethod]
     */
    private fun getMethod(apiClass: Class<*>, requestPath: String, requestHttpMethod: String): Method? =
        apiClass.methods.find { isUrlMatch(requestPath, getUrlFromMethod(it, requestHttpMethod)) }

    /**
     * Get url from method by looking into retrofit's annotations "value" attribute
     */
    private fun getUrlFromMethod(method: Method, requestHttpMethod: String): String? = when (requestHttpMethod) {
        METHOD_GET -> method.getAnnotation<GET>()?.let { return it.value }
        METHOD_POST -> method.getAnnotation<POST>()?.let { return it.value }
        METHOD_PUT -> method.getAnnotation<PUT>()?.let { return it.value }
        METHOD_PATCH -> method.getAnnotation<PATCH>()?.let { return it.value }
        METHOD_DELETE -> method.getAnnotation<DELETE>()?.let { return it.value }
        else -> null
    }

    /**
     * Check if [methodPath] match with [requestPath]
     */
    private fun isUrlMatch(requestPath: String, methodPath: String?): Boolean {
        methodPath ?: return false
        val requestPathSlices = requestPath.split(PATH_SEPARATOR)
        val methodPathSlices = methodPath.split(PATH_SEPARATOR)
        if (requestPathSlices.size != methodPathSlices.size) return false

        return methodPathSlices.mapIndexed { idx, it ->
            it.matches(BRACES_BLOCK.toRegex()) || it == requestPathSlices.getOrNull(idx)
        }.all { it }
    }
}

/**
 * Get annotation on method if it exists, and directly cast it
 */
inline fun <reified T> Method.getAnnotation() = annotations.find { it is T } as? T