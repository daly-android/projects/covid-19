package com.daly.covid19.common

import com.daly.covid19.common.core.implementations.FrameworkRepository
import com.daly.covid19.common.core.interfaces.IKoinModule
import com.daly.covid19.common.core.interfaces.IResourcesRepository
import org.koin.core.module.Module
import org.koin.dsl.module

object KoinModuleCommon : IKoinModule {

    /** Single instance of Framework implementation to fetch resources  */
    private val resourcesModule = module {
        single<IResourcesRepository> { FrameworkRepository(get()) }
    }

    override val modules: List<Module> = listOf(resourcesModule)
}