package com.daly.covid19.common.mock

import android.content.Context
import com.daly.covid19.common.monitoring.Monitor.logD
import com.daly.covid19.common.monitoring.Monitor.logE
import java.io.FileNotFoundException
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.Locale
import org.koin.core.KoinComponent
import uk.co.jemos.podam.api.PodamFactoryImpl

object MockGenerator : KoinComponent {

    private const val DOT = "."
    private const val UNDERSCORE = "_"
    private const val RAW_FOLDER = "raw"
    private const val LIST_PREFIX = "list_"
    private const val MOCK_PREFIX = "mock_"

    /**
     * Create an instance of [classToMock] and fill it with random data
     */
    fun createRandomMock(classToMock: Type): Any {
        val podamFactoryImpl = PodamFactoryImpl()
        return if (classToMock is ParameterizedType) {
            // in this case, we only have [List] so create one with two mocked objects
            val type = classToMock.actualTypeArguments[0] as Class<*>
            val list = mutableListOf(
                podamFactoryImpl.manufacturePojoWithFullData(type),
                podamFactoryImpl.manufacturePojoWithFullData(type)
            )
            list
        } else {
            podamFactoryImpl.manufacturePojoWithFullData(classToMock as Class<*>)
        }
    }

    /**
     * Get mock saved into res/raw if it exists, or null
     */
    fun getSavedMockFromClassType(context: Context, classToMock: Type): String? {
        val className = (
            if (classToMock is ParameterizedType) {
                LIST_PREFIX + (classToMock.actualTypeArguments[0] as Class<*>).name
            } else {
                (classToMock as Class<*>).name
            }
            )
            .lowercase(Locale.ROOT)
            .replace(DOT, UNDERSCORE)

        val mockFileName = "$MOCK_PREFIX$className"
        logD("Try to open $mockFileName")

        var mockedResult: String? = null

        val resId = context.resources.getIdentifier(mockFileName, RAW_FOLDER, context.packageName)
        if (resId > 0) {
            try {
                context.resources.openRawResource(resId).bufferedReader().use {
                    val content = it.readText()
                    if (content.isNotEmpty()) {
                        mockedResult = content
                    }
                }
            } catch (e: FileNotFoundException) {
                logE("Unable to open res file : ${e.message}")
            }
        } else {
            logE("Can't find res mock file named $mockFileName")
        }

        return mockedResult
    }

    /**
     * Get mock saved into res/raw if it exists, or null
     */
    fun getSavedMockFromFileName(context: Context, mockFileName: String): String? {
        logD("Try to open $mockFileName")

        var mockedResult: String? = null

        val resId = context.resources.getIdentifier(mockFileName, RAW_FOLDER, context.packageName)
        if (resId > 0) {
            try {
                context.resources.openRawResource(resId).bufferedReader().use {
                    val content = it.readText()
                    if (content.isNotEmpty()) {
                        mockedResult = content
                    }
                }
            } catch (e: FileNotFoundException) {
                logE("Unable to open res file : ${e.message}")
            }
        } else {
            logE("Can't find res mock file named $mockFileName")
        }

        return mockedResult
    }
}