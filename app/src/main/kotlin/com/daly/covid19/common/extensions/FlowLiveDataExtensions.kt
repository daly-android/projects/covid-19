package com.daly.covid19.common.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 * Transform a liveData to a [Flow] and listen for N’th updates.
 * @param listen n’th updates on LiveData before terminating the flow
 */
fun <T> LiveData<T>.asFlow(listen: Int = 1): Flow<T> {
    val resultChannel = Channel<T>(Channel.BUFFERED) // A buffer is absolutely necessary.
    val checkObserver = Observer<T> { resultChannel.trySend(it) }
    observeForever(checkObserver)
    return flow { // create flow after the observe function to not miss any value
        repeat(listen) { emit(resultChannel.receive()) }
        removeObserver(checkObserver)
    }
}