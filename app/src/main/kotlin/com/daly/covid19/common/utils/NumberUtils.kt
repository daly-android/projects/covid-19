package com.daly.covid19.common.utils

import java.text.NumberFormat
import java.util.Locale

/**
 *
 * Utils to format numbers
 */
class NumberUtils {

    companion object {

        private const val DEFAULT_FRACTION_DIGITS = 1

        /**
         * Format [Double] value with a fixed number of fraction digits
         *
         * By default [Locale.FRANCE] will be used
         *
         * @param value number (Double) to format
         * @param fractionDigits number of fraction digits
         */
        fun formatFixedFractionDigits(value: Double, fractionDigits: Int = DEFAULT_FRACTION_DIGITS): String {
            val numberFormat: NumberFormat = NumberFormat.getInstance(Locale.ROOT).apply {
                minimumFractionDigits = fractionDigits
                maximumFractionDigits = fractionDigits
            }
            return numberFormat.format(value)
        }
    }
}