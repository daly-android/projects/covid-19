package com.daly.covid19.common.core.interfaces

import org.koin.core.module.Module

interface IKoinModule {
    val modules: List<Module>
}