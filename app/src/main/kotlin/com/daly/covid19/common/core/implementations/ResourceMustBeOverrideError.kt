package com.daly.covid19.common.core.implementations

class ResourceMustBeOverrideError(private val resource: String) : Error("$resource must be override in resources")