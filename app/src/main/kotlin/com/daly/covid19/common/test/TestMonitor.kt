package com.daly.covid19.common.test

import com.daly.covid19.common.monitoring.IMonitor

/**
 * Test implementation of [IMonitor.LoggingAction] that use println
 * Used on unit tests
 */
class TestMonitor : IMonitor.LoggingAction() {

    companion object {
        private const val PREFIX = "CONVID19-TEST"
        private const val PREFIX_VERBOSE = "V/$PREFIX"
        private const val PREFIX_DEBUG = "D/$PREFIX"
        private const val PREFIX_WARNING = "W/$PREFIX"
        private const val PREFIX_ERROR = "E/$PREFIX"
    }

    override fun logV(message: String) {
        println("$PREFIX_VERBOSE : $message")
    }

    override fun logD(message: String) {
        println("$PREFIX_DEBUG : $message")
    }

    override fun logW(message: String) {
        println("$PREFIX_WARNING : $message")
    }

    override fun logE(message: String, throwable: Throwable?) {
        println("$PREFIX_ERROR : $message")
        throwable?.let {
            println("$PREFIX_ERROR : ${throwable.message}")
        }
    }
}