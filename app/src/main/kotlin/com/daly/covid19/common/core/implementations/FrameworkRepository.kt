package com.daly.covid19.common.core.implementations

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import android.util.TypedValue
import androidx.annotation.ArrayRes
import androidx.annotation.AttrRes
import androidx.annotation.BoolRes
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.content.res.ResourcesCompat.ID_NULL
import com.daly.covid19.common.core.interfaces.IResourcesRepository
import java.lang.IllegalStateException

/**
 * [IResourcesRepository] implementation for framework usage
 * @param appContext injected to an android appContext to fetch strings in XML file
 * Warning ! This class needs a call to theme.applyStyle(R.style.AppTheme, true) to properly work with attribute resolving and throws IllegalArgumentException if not
 */
class FrameworkRepository(private val appContext: Context) : IResourcesRepository {

    /**
     * Fetch a string in string.xml
     * @param id The identifier of the string resources
     * @param args An undefined number of arguments that an implement could use to fetch the data
     */
    override fun fetchString(@StringRes id: Int, vararg args: Any?): String = appContext.getString(id, *args)

    /**
     * Fetch a string in string.xml. It will throw an [ResourceMustBeOverrideError] if string result is empty
     * @param id The identifier of the string resources
     * @param args An undefined number of arguments that an implement could use to fetch the data
     */
    override fun fetchRequiredString(@StringRes id: Int, vararg args: Any?): String = fetchString(id, *args).also {
        if (it.isEmpty()) throw ResourceMustBeOverrideError(appContext.resources.getResourceEntryName(id))
    }

    /**
     * Fetch a string array in xml file
     * @param id The identifier of the string resources
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    override fun fetchStringArray(id: Int, vararg args: Any?): Array<out String> = appContext.resources.getStringArray(id)

    /**
     * Fetch a string array in xml file. It will throw an [ResourceMustBeOverrideError] array result is empty
     * @param id The identifier of the string resources
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    override fun fetchRequiredStringArray(id: Int, vararg args: Any?): Array<out String> = fetchStringArray(id, *args).also {
        if (it.isEmpty()) throw ResourceMustBeOverrideError(appContext.resources.getResourceEntryName(id))
    }

    /**
     * Fetch an array in xml file
     * @param id The identifier of the array resources
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    override fun fetchTypedArray(@ArrayRes id: Int, vararg args: Any?): TypedArray = appContext.resources.obtainTypedArray(id)

    /**
     * Fetch a drawable according to an id
     * We suppress the warning for not using ContextCompat because we use an api 23 that accept appContext.getDrawable
     * @param id The identifier of the drawable resources, may be a R.drawable or database identifier etc.
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    override fun fetchDrawable(@DrawableRes id: Int, vararg args: Any?): Drawable? = appContext.getDrawable(id)

    /**
     * Fetch a boolean according to an id
     * @param id The identifier of the boolean resources be a R.bool
     */
    override fun fetchBoolean(@BoolRes id: Int) = appContext.resources.getBoolean(id)

    /**
     * Fetch a color according to an id
     * @param id The identifier of the color resources be a R.color
     */
    override fun fetchColor(@ColorRes id: Int): Int = ResourcesCompat.getColor(appContext.resources, id, appContext.theme)

    /**
     * Fetch a plurals string according to an id and a quantity
     * @param id The identifier of the plurals resources
     * @param quantity The counter to select the correct string to return
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    override fun fetchQuantityString(@PluralsRes id: Int, quantity: Int, vararg args: Any?) = appContext.resources.getQuantityString(id, quantity, *args)

    /**
     * Fetch a plurals string according to an id and a quantity. It will throw an [ResourceMustBeOverrideError] string result is empty
     * @param id The identifier of the plurals resources
     * @param quantity The counter to select the correct string to return
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    override fun fetchRequiredQuantityString(@PluralsRes id: Int, quantity: Int, vararg args: Any?) = fetchQuantityString(id, quantity, *args).also {
        if (it.isEmpty()) throw ResourceMustBeOverrideError(appContext.resources.getResourceEntryName(id))
    }

    /**
     * Fetch a resource identifier for the given resource name
     * @param name The name of the desired resource.
     * @param type Optional default resource type to find, if "type/" is
     *                not included in the name.  Can be null to require an
     *                explicit type.
     * @param packageName Optional default package to find, if "package:" is
     *                   not included in the name.  Can be null to require an
     *                   explicit package.
     * @return int The associated resource identifier.  Returns 0 if no such
     *         resource was found.  (0 is not a valid resource ID.)
     */
    override fun fetchIdentifier(name: String, type: String, packageName: String) = appContext.resources.getIdentifier(name, type, packageName)

    /**
     * Fetch a integer associated with a particular resource ID.
     * @param id The identifier of the integer resources be a R.integer
     * */
    override fun fetchInteger(id: Int): Int = appContext.resources.getInteger(id)

    /**
     * Fetch a dimen associated with a particular resource ID.
     * @param id The identifier of the dimen resources be a R.dimen
     * */
    override fun fetchDimen(id: Int): Float = appContext.resources.getDimension(id)

    /**
     * Fetch an attribute resource from the current theme.
     * Attribute resource can be anything. This method fetch any kind of attribute into [typedValue]
     * This method is kinda generic because there is too much possible reference to fetch.
     * color -  dimen - font - string - float - int - colorStateList - array - ...
     *
     * Some common fetcher are defined in this interface like [fetchColorAttribute] or [fetchDrawableAttribute]
     *
     * @param attr attribute to fetch (can be color, dimen, stringRes, font, id, etc.)
     * @param typedValue holder of fetched resource.
     * @param resolveReference it will automatically resolve attr with format="reference"
     *        As example, you can't resolve a font directly into a typedValue you want the reference to that font.
     *        Some references can be resolved directly as int, dimen, color.
     */
    override fun resolveThemeAttribute(@AttrRes attr: Int, typedValue: TypedValue, resolveReference: Boolean) =
        appContext.theme.resolveAttribute(attr, typedValue, resolveReference)

    /**
     * Resolve a color by attribute
     * @param colorAttr attribute of a color resource
     * @return Color as int.
     * @throws IllegalArgumentException if [colorAttr] can't be resolved
     */
    override fun fetchColorAttribute(@AttrRes colorAttr: Int): Int = TypedValue().let {
        if (!resolveThemeAttribute(colorAttr, it, true)) throw IllegalStateException("Cannot resolve attribute")
        val isColor = TypedValue.TYPE_FIRST_COLOR_INT <= it.type && it.type <= TypedValue.TYPE_LAST_COLOR_INT
        if (!isColor) throw IllegalArgumentException("Attribute isn't a color")
        it.data
    }

    /**
     * Resolve a drawable by attribute or null if cannot succeed, never throw here !
     * Showing the UI without icon is better than make the app crash
     * @param drawableAttr attribute of a drawable resource
     * @return resolved drawable
     */
    override fun fetchDrawableAttribute(@AttrRes drawableAttr: Int): Drawable? = TypedValue().let {
        if (!resolveThemeAttribute(drawableAttr, it, false)) throw IllegalStateException("Cannot resolve attribute")
        if (it.type != TypedValue.TYPE_REFERENCE) throw IllegalArgumentException("Attribute isn't a reference")
        if (it.data == ID_NULL) return null
        fetchDrawable(it.data)
    }

    /**
     * Resolve a dimen by attribute
     * @param dimenAttr attribute of a dimen resource
     * @return resolved dimension
     * @throws IllegalArgumentException if [dimenAttr] can't be resolved
     */
    override fun fetchDimenAttribute(@AttrRes dimenAttr: Int): Float = TypedValue().let {
        if (!resolveThemeAttribute(dimenAttr, it, true)) throw IllegalStateException("Cannot resolve attribute")
        if (it.type != TypedValue.TYPE_DIMENSION) throw IllegalArgumentException("Attribute isn't a dimension")
        it.getDimension(Resources.getSystem().displayMetrics)
    }

    /**
     * Resolve a string by attribute
     * @param stringAttr attribute of a string resource
     * @return resolved string
     * @throws IllegalArgumentException if [stringAttr] can't be resolved
     */
    override fun fetchStringAttribute(@AttrRes stringAttr: Int): CharSequence = TypedValue().let {
        if (!resolveThemeAttribute(stringAttr, it, true)) throw IllegalStateException("Cannot resolve attribute")
        if (it.type != TypedValue.TYPE_STRING) throw IllegalArgumentException("Attribute isn't a string")
        it.string
    }
}