package com.daly.covid19.common.uikit.lastchange

import androidx.lifecycle.MutableLiveData

/**
 * ViewData of the [LastChangeView] component
 */
class LastChangeViewData {
    val lastChangeText = MutableLiveData<String?>()
}