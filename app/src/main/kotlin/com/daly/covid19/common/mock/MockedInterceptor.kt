package com.daly.covid19.common.mock

import android.content.Context
import com.daly.covid19.R
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody

/**
 * Interceptor used to mock API responses
 * It's used when the boolean "is_mock_enabled" is set to "true" (in res/values/bools.xml)
 *
 * It try to returns a mock saved in debug/res/raw/mock_(list_)<class>.json (ex: mock_list_com_daly_covid19_overview_data_models_latesttotals.json)
 * If expected API result is a list, you have to prefix class by "list_"
 * If this file doesn't exists, it returns a random mocked response
 */
class MockedInterceptor(
    private val apiClass: Class<*>,
    private val context: Context,
) : Interceptor {

    companion object {
        private const val MEDIA_TYPE = "application/json"
        private const val CONTENT_TYPE_HEADER_NAME = "content-type"
        private const val SUCCESS_CODE = 200
        private const val EMPTY_JSON = "{}"
        private const val EMPTY = ""
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestUri = chain.request().url.toUri().toString()
        val method = chain.request().method

        var responseString = EMPTY_JSON
        val baseUrl = context.getString(R.string.covid_data_base_url)
        val requestPath = requestUri.replace(baseUrl, EMPTY)
        RetrofitTypeUtils.getExpectedTypeResultFromUri(apiClass, requestPath, method)?.let { type ->
            MockGenerator.getSavedMockFromClassType(context, type)?.let {
                // returns mock saved into res file
                responseString = it
            } ?: run {
                // returns a random mock
                val mockedObject = MockGenerator.createRandomMock(type)

                // here we use Gson instead of Moshi because Moshi can't create json without knowing object's type
                responseString = GsonBuilder().create().toJson(mockedObject)
            }
        }

        return chain.proceed(chain.request())
            .newBuilder()
            .code(SUCCESS_CODE)
            .message(responseString)
            .body(
                responseString.toByteArray()
                    .toResponseBody(MEDIA_TYPE.toMediaTypeOrNull())
            )
            .addHeader(CONTENT_TYPE_HEADER_NAME, MEDIA_TYPE)
            .build()
    }
}