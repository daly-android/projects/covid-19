package com.daly.covid19.overview.data.common.utils

import okhttp3.Request

/**
 * Inject an authorization api key in request
 *
 * @param request the request to send
 * @param rapidApiKey the API Key to inject
 * @return the request with the credentials
 */
fun injectXRapidApiKey(request: Request, rapidApiKey: String): Request {
    return request.newBuilder()
        .header("x-rapidapi-key", rapidApiKey)
        .build()
}

/**
 * Inject an authorization api host in request
 *
 * @param request the request to send
 * @param rapidApiHost The API Host to inject
 * @return the request with the credentials
 */
fun injectXRapidApiHost(request: Request, rapidApiHost: String): Request {
    return request.newBuilder()
        .header("x-rapidapi-host", rapidApiHost)
        .build()
}