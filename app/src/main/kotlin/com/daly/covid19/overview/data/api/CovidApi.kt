package com.daly.covid19.overview.data.api

import com.daly.covid19.overview.data.models.LatestTotals
import retrofit2.Call
import retrofit2.http.GET

interface CovidApi {

    /**
     * Get latest data for whole world.
     *
     * @return [Call]<[kotlin.collections.List<LatestTotals>]>
     */

    @GET("/totals?format=json")
    fun getLatestTotalsUsingGET(): Call<List<LatestTotals>>
}