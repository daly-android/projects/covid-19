package com.daly.covid19.overview.presentation.views

import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.View
import android.view.animation.AnticipateInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.doOnEnd
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.daly.covid19.R
import com.daly.covid19.common.core.interfaces.IRouter
import com.daly.covid19.overview.presentation.viewmodels.MainActivityViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val router: IRouter by inject()

    private val viewModel: MainActivityViewModel by viewModel()

    /**
     * Load MainActivityViewModel when MainActivity is created
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        // SplashScreen Icon not displayed when launch app from Android Studio : https://issuetracker.google.com/issues/207386164
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        // Configure SplashScreen
        with(splashScreen) {
            // Configure SplashScreen visibility condition
            setKeepOnScreenCondition { !viewModel.isReady }

            // Configure SplashScreen exit animation
            setOnExitAnimationListener { splashScreenProvider ->
                val splashScreenView = splashScreenProvider.view

                // Create your custom animation.
                val slideUp = ObjectAnimator.ofFloat(
                    splashScreenView,
                    View.TRANSLATION_Y,
                    0f,
                    -splashScreenView.height.toFloat()
                )
                slideUp.interpolator = AnticipateInterpolator()

                // Call SplashScreenView.remove at the end of your custom animation.
                slideUp.doOnEnd { splashScreenProvider.remove() }

                // Run your animation.
                slideUp.start()
            }
        }

        // Initialize navigation router for this activity
        router.init(
            mapOf(
                IRouter.FRAGMENT_MANAGER to supportFragmentManager,
                IRouter.CONTAINER_ID to R.id.main_container
            )
        )

        // Load MainActivityViewModel
        viewModel.load(this)
    }
}