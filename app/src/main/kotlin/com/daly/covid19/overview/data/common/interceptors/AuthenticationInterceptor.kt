package com.daly.covid19.overview.data.common.interceptors

import com.daly.covid19.overview.data.common.utils.injectXRapidApiHost
import com.daly.covid19.overview.data.common.utils.injectXRapidApiKey
import okhttp3.Interceptor
import okhttp3.Response

class AuthenticationInterceptor : Interceptor {

    /**
     * External function used to get API KEY from c library
     * See https://www.geeksforgeeks.org/securing-api-keys-using-android-ndk/
     */
    private external fun getApiKey(): String

    companion object {
        private const val COVID_DATA_RAPID_API_HOST = "covid-19-data.p.rapidapi.com"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = injectXRapidApiKey(request, getApiKey())
        request = injectXRapidApiHost(request, COVID_DATA_RAPID_API_HOST)
        return chain.proceed(request)
    }
}