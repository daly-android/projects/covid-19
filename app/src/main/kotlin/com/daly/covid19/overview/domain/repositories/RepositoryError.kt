package com.daly.covid19.overview.domain.repositories

sealed class RepositoryError {
    object NotAuthorized : RepositoryError()
    object Internal : RepositoryError()
    object GenericFailure : RepositoryError()
}