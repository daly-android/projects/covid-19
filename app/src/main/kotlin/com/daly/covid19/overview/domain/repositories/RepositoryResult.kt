package com.daly.covid19.overview.domain.repositories

sealed class RepositoryResult {
    data class Success<T>(val data: T) : RepositoryResult()
    data class Failure(val error: RepositoryError) : RepositoryResult()
}