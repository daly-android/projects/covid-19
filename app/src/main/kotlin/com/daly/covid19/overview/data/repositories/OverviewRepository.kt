package com.daly.covid19.overview.data.repositories

import com.daly.covid19.common.monitoring.Monitor.logE
import com.daly.covid19.overview.data.api.CovidApi
import com.daly.covid19.overview.data.common.call.ApiError
import com.daly.covid19.overview.data.common.call.ApiResult
import com.daly.covid19.overview.data.common.call.quickCallApi
import com.daly.covid19.overview.data.mappers.toDomainModel
import com.daly.covid19.overview.domain.repositories.IOverviewRepository
import com.daly.covid19.overview.domain.repositories.RepositoryError
import com.daly.covid19.overview.domain.repositories.RepositoryResult
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/** @see IOverviewRepository */
@OptIn(ExperimentalCoroutinesApi::class)
class OverviewRepository(
    private val covidApi: CovidApi
) : IOverviewRepository {
    /** @see IOverviewRepository.fetchLatestTotals */
    override fun fetchLatestTotals(): Flow<RepositoryResult?> = flow {
        logE(::fetchLatestTotals.name)
        when (val response = covidApi.getLatestTotalsUsingGET().quickCallApi()) {
            is ApiResult.Success -> emit(RepositoryResult.Success(response.body?.get(0)?.toDomainModel()))
            is ApiResult.Error -> {
                logE("fetch latest totals failed : ${response.error.message}")
                when (response.error) {
                    is ApiError.Internal -> emit(RepositoryResult.Failure(RepositoryError.Internal))
                    is ApiError.Failure -> {
                        // FIXME Handle RepositoryError.GenericFailure
                        //   emit RepositoryError.NotAuthorized just when error code is 401
                        emit(RepositoryResult.Failure(RepositoryError.NotAuthorized))
                    }
                }
            }
        }
    }
}