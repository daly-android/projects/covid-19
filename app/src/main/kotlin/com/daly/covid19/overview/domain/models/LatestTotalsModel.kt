package com.daly.covid19.overview.domain.models

/**
 * Model object of a latest totals.
 */
data class LatestTotalsModel(
    val categories: List<CategoryInfo>,
    val lastChange: String?,
    val lastUpdate: String?,
) {
    data class CategoryInfo(
        val category: Category,
        val value: Long
    )

    enum class Category {
        CONFIRMED, RECOVERED, CRITICAL, DEATHS
    }
}