package com.daly.covid19

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.daly.covid19.common.core.interfaces.IRouter
import com.daly.covid19.common.monitoring.Monitor.logD
import com.daly.covid19.overview.presentation.views.OverviewFragment
import kotlin.system.exitProcess

/**
 * Coordinator who takes care of the navigation in the application
 * Ex: navigation between fragments, go third party app, outgoing ...
 */
class AppCoordinator(private val router: IRouter) {

    fun gotToOverview() {
        logD(::gotToOverview.name)
        router.show(fragment = OverviewFragment::class.java)
    }

    fun showNetworkUnavailableAlertDialog(context: Context) {
        AlertDialog.Builder(context).setTitle("No Internet Connection")
            .setCancelable(false)
            .setMessage("Please check your internet connection and try again")
            .setPositiveButton(android.R.string.ok) { _, _ -> exitProcess(0) }
            .setIcon(android.R.drawable.ic_dialog_alert).show()
    }
}