## CategoryLatestTotalsView

![component_image](view_totals_category.png)

An icon with two texts below (title and value).

## XML use

You must provide a lifecycle and an object of the type `com.daly.covid19.common.uikit.category.CategoryLatestTotalsView` 

```xml
<com.daly.covid19.common.uikit.category.CategoryLatestTotalsView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:lifecycle="@{lifecycle}"
    app:viewData="@{viewModel.categoryLatestTotalsViewData}" />
```

## ViewData use

| Data | Type | Description |
|---|---|---|
| categoryIconSrc               | Drawable *      | Icon source of the component  |
| categoryIconBackground              | Drawable *      | Icon background color of the component |
| categoryIconTint  | Int *      | Icon content color of the component |
| categoryTitle | Int *      | Text displayed on line 1 of the component  |
| categoryValue               | Int *     | Text displayed on line 2 of the component  |
    
* `MutableLiveData<T>`

## Example

```kotlin
val confirmedLatestTotals = CategoryLatestTotalsViewData().apply {
    categoryIconSrc.postValue(ContextCompat.getDrawable(context, R.drawable.ic_confirmed))
    categoryIconBackground.postValue(ContextCompat.getDrawable(context, R.drawable.background_confirmed_icon))
    categoryIconTint.postValue(ContextCompat.getColor(context, R.color.white))
    categoryTitle.postValue(context.getString(R.string.confirmed))
    categoryValue.postValue(CategoryUtils.insertSpacesBetweenNumbers(categoryInfo.value.toString()))
}
```
