## LastChangeView

![component_image](view_last_change.png)

Simple box with two lines of text.

## XML use

You must provide a lifecycle and an object of the type `com.daly.covid19.common.uikit.lastchange.LastChangeView` 

```xml
<com.daly.covid19.common.uikit.lastchange.LastChangeView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:lifecycle="@{lifecycle}"
    app:viewData="@{viewModel.lastChangeViewData}" />
```

## ViewData use

| Data | Type | Description |
|---|---|---|
| lastChangeText | String *      | Text displayed on line 2 of the component  |
    
* `MutableLiveData<T>`

## Example

```kotlin
val lastChange = LastChangeViewData().apply {
    lastChangeText.postValue("2021-05-04T12:46:05+02:00")
}
```
